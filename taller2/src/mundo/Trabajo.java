package mundo;

import java.util.Date;

import mundo.Ocio.TipoEventoOcio;

public class Trabajo extends Evento{
	
	public enum TipoDeEvento{
		JUNTA, 
		ALMUERZO, 
		CENA, 
		COCTEL, 
		PRESNTEACION,
		OTRO;
	}
	
	protected TipoDeEvento tipo;

	public Trabajo(Date pFecha, String pLugar, boolean pObligatorio,boolean pFormal, TipoDeEvento pTipo) {
		super(pFecha, pLugar, pObligatorio, pFormal);
		tipo=pTipo;
	}

}
