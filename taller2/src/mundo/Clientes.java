package mundo;

import java.util.Date;

public class Clientes extends Trabajo {

	protected String empresa;
	
	public Clientes(Date pFecha, String pLugar, boolean pObligatorio, boolean pFormal, TipoDeEvento pTipo, String pEmpresa) {
		super(pFecha, pLugar, pObligatorio, pFormal, pTipo);
		empresa=pEmpresa;
	}
	
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}

		public String toString()
	{
			return "Evento con CLIENTES: \n"
			  		+"FECHA: " + this.fecha
			  		+"\nLUGAR: " + this.lugar
			  		+"\nTIPO EVENTO: " + this.tipo
			  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
			  		+"\nFORMAL: "+ conv(this.formal);
	}



}
