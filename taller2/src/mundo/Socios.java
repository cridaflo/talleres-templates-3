package mundo;

import java.util.Date;

public class Socios extends Trabajo {

	protected String Empresa;
	public Socios(Date pFecha, String pLugar, boolean pObligatorio,	boolean pFormal, TipoDeEvento pTipo, String pEmpresa) {
		super(pFecha, pLugar, pObligatorio, pFormal, pTipo);
		Empresa=pEmpresa;
	}
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString()
	{
	  return "Evento con SOCIOS: \n"
	  		+"FECHA: " + this.fecha
	  		+"\nLUGAR: " + this.lugar
	  		+"\nTIPO EVENTO: " + this.tipo
	  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
	  		+"\nFORMAL: "+ conv(this.formal);
	}

	
}
