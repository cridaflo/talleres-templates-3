package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	/**
	 * Número de columnas del tablero
	 */
	private int columnas;

	/**
	 * Número de fias del tablero
	 */
	private int filas;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Jugador en turno
	 */
	private Jugador jugadorActual;
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	/**
	 * Número de fichas consecuiva para ganar.
	 */
	private int fichasVictoria;

	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol, int pFichas)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		filas=pFil;
		columnas=pCol;
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
		jugadorActual=jugadores.get(turno);
		fichasVictoria=pFichas;
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el turno ue se juega actualmente;
	 */
	public int darTurno(){
		return turno;
	}
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{

		return atacante;
	}
	
	/**
	 * Retorna el número de filas.
	 */
	public int darFilas(){
		return filas;
	}
	
	/**
	 * Retorna el número de columnas.
	 */
	public int darColumnas(){
		return columnas;
	}
	
	/**
	 * 
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		Random r = new Random();
		int n = r.nextInt(columnas)+1;
		boolean jugada=registrarJugada(n);
		while(!jugada){
			n=r.nextInt(columnas)+1;
			jugada=registrarJugada(n);
		}

	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		col--;
		System.out.println("=)"+col);
		if(!tablero[0][col].equals("___")){
			return false;
		}
		else{
			int fila=filas-1;
			turno++;
			boolean jugo=false;
			for(int i=0; i<filas-1 && !jugo; i++){
				if(!tablero[i+1][col].equals("___")){
					tablero[i][col]=jugadorActual.darSimbolo();
					fila=i;
					jugo=true;
				}
			}
			if(!jugo){
				tablero[filas-1][col]=jugadorActual.darSimbolo();
			}
			finJuego=terminar(fila, col);
			if(!finJuego){
				cambiarJugador();
			}
			return true;
		}
	}


	/**
	 * Actualiza el jugador en turno
	 */
	public void cambiarJugador() {
		int tam=jugadores.size();
		jugadorActual=jugadores.get(turno%tam);
		atacante=jugadorActual.darNombre();


	}
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{

		boolean rta=false;
		if(!tablero[fil][col].equals("___")){		
			String[][] tablero2=tablero;
			tablero2[fil][col]=jugadorActual.darSimbolo();
			rta=terminaDiagonalNegativa(fil, col, tablero2)||terminaDiagonalPositiva(fil, col, tablero2)||terminaHorizontal(fil, col, tablero2)||terminaVertical(fil, col, tablero2);
		}

		return rta;
	}
	/**
	 * Determina si una jugada logra 4 o más fichas en línea horizontalmente.
	 * @return true si la jugada termina el juego false de lo contrario.
	 */
	public boolean terminaHorizontal(int fil, int col, String[][] pTablero){
		boolean rta=false;
		String simbolo=pTablero[fil][col];
		int n=0;
		for(int i=col; i<columnas && pTablero[fil][i].equals(simbolo); i++){
			n++;
		}
		for(int i=col-1; i>=0 && pTablero[fil][i].equals(simbolo); i--){
			n++;
		}
		if(n>=fichasVictoria){
			rta=true;
		}

		return rta;

	}

	/**
	 * Determina si una jugada logra 4 o más fichas en línea verticalmente.
	 * @return true si la jugada termina el juego false de lo contrario.
	 */
	public boolean terminaVertical(int fil, int col, String[][] pTablero){
		boolean rta=false;
		String simbolo=pTablero[fil][col];
		int n=0;
		for(int i=fil; i<filas && pTablero[i][col].equals(simbolo); i++){
			n++;
		}
		if(n>=fichasVictoria){
			rta=true;
		}

		return rta;
	}
	/**
	 * Determina si una jugada logra 4 o más fichas en línea en la diagonal con pendiente negativa.
	 * @return true si la jugada termina el juego false de lo contrario.
	 */
	public boolean terminaDiagonalNegativa(int fil, int col, String[][] pTablero){
		boolean rta=false;
		String simbolo=pTablero[fil][col];
		int n=0;
		for(int i=fil, j=col;  i<filas && j<columnas && pTablero[i][j].equals(simbolo) ; i++, j++){
			n++;
		}
		for(int i=fil-1, j=col-1;i>=0 && j>=0 && pTablero[i][j].equals(simbolo); i--, j--){
			n++;
		}
		if(n>=fichasVictoria){
			rta=true;
		}	
		return rta;
	}
	/**
	 * Determina si una jugada logra 4 o más fichas en línea en la diagonal con pendiente negativa.
	 * @return true si la jugada termina el juego false de lo contrario.
	 */
	public boolean terminaDiagonalPositiva(int fil, int col, String[][] pTablero){
		boolean rta=false;
		String simbolo=pTablero[fil][col];
		int n=0;
		for(int i=fil, j=col; i>=0 && j<columnas &&  pTablero[i][j].equals(simbolo); i--, j++){
			n++;
		}
		for(int i=fil+1, j=col-1;  i<filas && j>=0 && pTablero[i][j].equals(simbolo); i++, j--){
			n++;
		}
		if(n>=fichasVictoria){
			rta=true;
		}	
		return rta;
	}
	


}
