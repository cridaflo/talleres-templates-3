package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		boolean valido = false;
		int filas =0;
		while(!valido){
			try{
				System.out.println("Número de filas del tablero");
				filas = Integer.parseInt(sc.next());
				valido=true;
			}
			catch(Exception e){
				System.out.println("El valor ingresado no es válido");
			}
		}
		valido=false;
		int columnas=0;
		while(!valido){
			try{
				System.out.println("Número de columnas del tablero");
				columnas=Integer.parseInt(sc.next());
				valido=true;
			}
			catch(Exception e){
				System.out.println("El valor ingresado no es válido");
			}
		}
		valido=false;
		int fichas =0;
		while(!valido){
			try{
				System.out.println("Número de fichas consecutivas para ganar");
				fichas = Integer.parseInt(sc.next());
				if(fichas>columnas && fichas>filas){
					System.out.println("El número de fichas consecutivas necesarias para ganar es mayor al posible según las dimesiones especificadas ");
				}
				else{
					valido=true;
				}
			}
			catch(Exception e){
				System.out.println("El valor ingresado no es válido");
			}

		}
		valido = false;
		ArrayList<Jugador> jugadores = new ArrayList<>();
		int numJugadores=0;
		while(!valido){
			try{
				System.out.println("Número de jugadores de la partida");
				numJugadores=Integer.parseInt(sc.next());
				valido=true;
			}
			catch(Exception e){
				System.out.println("El valor ingresado no es válido");
			}
		}

		for(int i =1; i<=numJugadores; i++){
			valido=false;
			System.out.println("Nombre del jugador "+i);
			String nombre=sc.next();
			String simbolo="s";
			while(!valido){	
				System.out.println("Símbolo del jugador "+i);
				simbolo=sc.next();
				char[] c =simbolo.toCharArray();
				if(c.length==1&& Character.isLetterOrDigit(c[0])){
					if(esSimboloDisponible(c[0],jugadores)){
						valido=true;	
					}
				}
				else
					System.out.println("El símbolo ingresado no es váldo, inténtelo nuevamente");
			}
			Jugador j = new Jugador(nombre, " "+simbolo+" ");
			jugadores.add(j);
		}
		juego=new LineaCuatro(jugadores, filas, columnas, fichas);
		juego();
	}

	public boolean esSimboloDisponible(char simbolo, ArrayList<Jugador> jugadores) {
		boolean rta=true;
		for(int i =0; i<jugadores.size()&& rta; i++){
			if(jugadores.get(i).darSimbolo().equals(" "+simbolo+" ")){
				rta=false;
				System.out.println("El símbolo ya fue escogido por otro jugador, seleccione otro símbolo");
			}
		}
		return rta;
	}
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		boolean empate =false;
		while(!juego.fin()){
			System.out.println("Turno de "+juego.darAtacante());
			System.out.println("Ingrese la columna en la cual desea ubicar la ficha");

			boolean jugo =false;
			int n =0;
			while(!jugo){
				if(n>0){
					System.out.println("La jugada ingresada no es válida, inténtelo nuevamente");
				}
				try{
					int col = Integer.parseInt(sc.next());
					jugo=juego.registrarJugada(col);
				}
				catch(Exception e){
					System.out.println("La jugada ingresada no es válida, inténtelo nuevamente");
				}
				imprimirTablero();
				n++;

			}
			if((juego.darFilas()*juego.darColumnas())==juego.darTurno()){
				empate=true;
				break;
			}
		}
		if(empate){
			System.out.println("La partida ha terminado en un empate.");
		}
		else{
			System.out.println("Felicidades!!!!\nEl jugador "+juego.darAtacante()+" a ganado la partida.");
		}
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		boolean valido=false;
		int filas =0;
		while(!valido){
			try{
				System.out.println("Número de filas del tablero");
				filas = Integer.parseInt(sc.next());
				valido=true;
			}
			catch(Exception e){
				System.out.println("El valor ingresado no es válido");
			}
		}
		valido=false;
		int columnas=0;
		while(!valido){
			try{
				System.out.println("Número de columnas del tablero");
				columnas=Integer.parseInt(sc.next());
				valido=true;
			}
			catch(Exception e){
				System.out.println("El valor ingresado no es válido");
			}
		}
		valido=false;
		int fichas =0;
		while(!valido){
			try{
				System.out.println("Número de fichas consecutivas para ganar");
				fichas = Integer.parseInt(sc.next());
				if(fichas>columnas && fichas>filas){
					System.out.println("El número de fichas consecutivas necesarias para ganar es mayor al posible según las dimesiones especificadas ");
				}
				else{
					valido=true;
				}
			}
			catch(Exception e){
				System.out.println("El valor ingresado no es válido");
			}
		}
		System.out.println("Nombre del jugador");
		String nombre=sc.nextLine();
		valido=false;
		String simbolo=sc.nextLine();
		while(!valido){	
			System.out.println("Símbolo del jugador");
			simbolo=sc.nextLine();
			char[] c =simbolo.toCharArray();
			if(c.length==1&& Character.isLetterOrDigit(c[0])){
				valido=true;
			}
			else
				System.out.println("El símbolo ingresado no es váldo, inténtelo nuevamente");
		}

		valido=false;
		int n =0;
		while(!valido){
			try{
				System.out.println("1. Primer turno");
				System.out.println("2. Segundo turno");
				n=Integer.parseInt(sc.next());
				System.out.println(n);
				if(n!=1 && n!= 2){
					throw new Exception();
				}
				valido=true;
			}
			catch(Exception e){
				System.out.println("Comando inválido");
			}

		}
		ArrayList<Jugador> jugador=new ArrayList<>();
		Jugador elJugador= new Jugador(nombre, " "+simbolo+" ");
		Jugador maquina= new Jugador("Maquina", " # ");
		if(n==1){
			jugador.add(elJugador);
			jugador.add(maquina);
		}
		else if (n==2){
			jugador.add(maquina);
			jugador.add(elJugador);
		}
		juego= new LineaCuatro(jugador, filas, columnas, fichas);
		juegoMaquina();

	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		while(true){
			if(juego.darAtacante().equals("Maquina")){
				System.out.println("Jugada de la maquina");
				juego.registrarJugadaAleatoria();
				imprimirTablero();
				if(juego.fin()){
					System.out.println("Ha pedido la partida\nInténtelo nuevamente");
				}
			}
			else{
				System.out.println("Turno de "+juego.darAtacante());
				System.out.println("Ingrese la columna en la cual desea ubicar la ficha");

				boolean jugo =false;
				int n =0;
				while(!jugo){
					if(n>0){
						System.out.println("La jugada ingresada no es válida, inténtelo nuevamente");
					}
					try{
						int col = Integer.parseInt(sc.next());
						jugo=juego.registrarJugada(col);
					}
					catch(Exception e){
						System.out.println("La jugada ingresada no es válida, inténtelo nuevamente");
					}
					imprimirTablero();
					n++;
				}	
				if(juego.fin()){
					System.out.println("Felicidades!!!\nHa ganado la partida");
					break;
				}
			}
			if((juego.darFilas()*juego.darColumnas())==juego.darTurno()){
				System.out.println("El juego ha terminado en un empate");
				break;
			}
		}
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String[][] tablero = juego.darTablero();
		int filas=juego.darFilas();
		int columnas=juego.darColumnas();
		for(int i=0; i<filas; i++){
			for(int j=0; j<columnas; j++){
				System.out.print(tablero[i][j]);
			}
			System.out.println();
		}
	}
}
